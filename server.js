var express = require("express"),
  app = express(),
  bodyParser = require("body-parser"),
  exphbs = require("express-handlebars"),
  movingToGL_appMetadata = require("./data");

// Pull metadata from the data.js
var tutorialStep1 = movingToGL_appMetadata.tutorial_PrepareYourProjects,
  tutorialGH = movingToGL_appMetadata.tutorial_github2gitlab,
  homepage = movingToGL_appMetadata.homepage,
  about = movingToGL_appMetadata.aboutPage,
  projectSlug = movingToGL_appMetadata.glitchProjectSlug,
  launchpadHomepage = movingToGL_appMetadata.launchpadPage;

var nodejsPlatformType = process.env.NODE_ENV;

if ((nodejsPlatformType === "production")) {
  console.info("You are running the server in production mode. Congrats!");
} else {
  console.warn(
    "To speed up your Express server, we recommend to go to production mode by changing the 'NODE_ENV' in your environment variables to 'production'."
  );
}

app.engine("handlebars", exphbs({ defaultLayout: "main" }));
app.set("views", __dirname + "/views");
app.set("view engine", "handlebars");

app.use(
  bodyParser.urlencoded({
    extended: true
  })
);

app.get("/", (req, res) => {
  res.render("home", {pageName: homepage, glitchProjectSlug: projectSlug});
})

app.get("/about", (req, res) => {
  res.render("about", {pageTitle: about, glitchProjectSlug: projectSlug });
});

app.get("/launchpad", (req, res) => {
  res.render("launchpad/index", {pageTitle: launchpadHomepage, glitchProjectSlug: projectSlug})
})

// Handle pings from checks
app.get("/ping", (req, res) => {
  res
    .status(200)
    .send({status: 200, description: "A ping was received, so the server uptime had extended by 5 minutes. If this was hosted outside Glitch, this is just checking the instance status."})
});

// Let Express handle the server error bugs.
app.use(function(err, req, res, next) {
  console.error(err.stack);
  res
    .status(500)
    .send({ error: 500, description: "Something went berserk on our side. Please contact Support or inspect the source code instead." });
});

app.get("/fork", (req, res) => {
  res.redirect("https://glitch.com/edit/#!/remix/movingtogitlab")
})

app.set("port", process.env.PORT);
app.set("ip", "127.0.0.1");

var listener = app.listen(process.env.PORT, function() {
  console.log("Your app is running on port " + listener.address().port);
});
