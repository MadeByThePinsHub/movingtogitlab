[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)


#MovingToGitLab Tutorials [Redesigned]
===========

The redesigned #MovingToGitLab tutorials now uses Handlebars to lessen the copy-paste-customize paperwork. This is where the #MovingToGitLab
lives on.

## Setup
1. Clone the whole repo with `git clone https://gitlab.com/MadeByThePinsTeam-DevLabs/movingtogitlab.git`.
2. Install dependencies with `npm install`.
3. Edit `data.js`
4. Open command-line interface and run `npm start` and hit the road!

## FAQs
### Why the changes doesn't appear immediately?
The **Refresh App on Changes** maybe disabled, so you need to refresh te page manually.
If you want to see chnges immediately, tick it in Editor Settings.

See [this Glitch Help Center article](https://glitch.com/help/refresh/) for more info on this flag.

### Why do you use Prettier?
We are using Prettier to format our JS and Handlebars code.

### PM2 what?
We are using PM2 to monitor this instance. Feel free to use other monitoring packages.

## Having Issues?
If you have issues on browsing the site, [please raise an new issue on GitLab].

<!-- Markdown links -->
[please raise an new issue on GitLab]: https://gitlab.com/MadeByThePinsTeam-DevLabs/movingtogitlab/issues/new