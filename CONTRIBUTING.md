# Contributors' Guide

Welcome to our Contributors' Guide! If you are new to this Glitch project and want to contribute

## TL;DR
- [Follow our tutorials style guide](#style-guide)
- [Keep it simple, stupid](https://en.handbooksbythepins.cf/style-guides/kiss-principle)
- [Avoid commiting your deepest secrets here](https://en.handbooksbythepins.cf/the-gitlab-way/your-deepest-secrets-are-worst-too)

## Style Guide
Our style guide for the tutorials must be have illustrations no higher than 10

## Need help?
We want

## Avoid Commiting Your Deepest Secrets