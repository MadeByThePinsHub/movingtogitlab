/**
  This is the data.js, where we store different variables for the usage of server.js file.
**/

var friendlyProjectId = process.env.PROJECT_DOMAIN

module.exports = {
  site_title: "#MovingToGitLab Tutorials",
  gl_issue_tracker:
    "https://gitlab.com/MadeByThePinsTeam-DevLabs/movingtogitlab/issues/new",
  glitchProjectDomain: "https://" + process.env.PROJECT_DOMAIN + ".glitch.me",
  glitchProjectSlug: friendlyProjectId || "movingtogitlab",
  homepage: "Welcome",
  aboutPage: "About",
  tutorial_PrepareYourProjects: "Prepare your projects for migration",
  tutorial_github2gitlab: "Migrating from GitHub",
  tutorial_bitbucket2gitlab: "Migrating from Bitbucket",
  tutorial_checklist: "Your Migration Checklist",
  launchpadPage: "Tutorial Launchpad",
};
